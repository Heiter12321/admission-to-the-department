#include <iostream>
#include <vector>
#include <string>
#include <cstring>

using std::cin;
using std::cout;
using std::vector;
using std::string;

int main() {
  cout << "Введите размер первого изображения:\n";
  int n = 0;
  int m = 0;
  cin >> n >> m;
  cout << "Введите числа в виде двумерного массива:\n";
  vector<vector<int>> first_image(n, vector<int>(m));
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < m; ++j) {
      cin >> first_image[i][j];
    }
  }

  cout << "Введите размер второго изображения:\n";
  int k = 0;
  int l = 0;
  cin >> k >> l;
  cout << "Введите числа в виде двумерного массива:\n";
  vector<vector<int>> second_image(k, vector<int>(l));
  for (int i = 0; i < k; ++i) {
    for (int j = 0; j < l; ++j) {
      cin >> second_image[i][j];
    }
  }

  cout << "Ваша первая картинка:\n";

  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < m; ++j) {
      cout << first_image[i][j] << " ";
    }
    cout << "\n";
  }

  cout << "\nВаша вторая картинка:\n";

  for (int i = 0; i < k; ++i) {
    for (int j = 0; j < l; ++j) {
      cout << second_image[i][j] << " ";
    }
    cout << "\n";
  }

  cout << "\nВведите желаемую точность(Точно или Ориентир):\n";

  char* str;
  cin >> str;

  if (strstr(str, "Ориентир") != NULL) {

  // Start hash-----------------------------------------------------------------------------------------------------------

    int simple_number1 = 271;
    int simple_number2 = 17;
    int module = 1000001;

    vector<int> first_hash(k);

    int current_multiplier_column = simple_number2;
    int all_sum = 0;
    for (int i = 0; i < k; ++i) {
      int current_multiplier_raw = simple_number1;
      int sum = 0;
      for (int j = 0; j < l; ++j) {
        sum = (sum + second_image[i][j] * current_multiplier_raw) % module;
        current_multiplier_raw = (simple_number1 * current_multiplier_raw) % module;
      }
      all_sum = (all_sum + sum * current_multiplier_column) % module;
      current_multiplier_column = (current_multiplier_column * simple_number2) % module;
    }

    int hash_of_second_image = all_sum;

    vector<vector<int>> temp_hash_matrix(n, vector<int>(m - l + 1));

    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < m - l + 1; ++j) {
        int current_multiplier_raw = simple_number1;
        int sum = 0;
        for (int t = 0; t < l; ++t) {
          sum = (sum + first_image[i][j + t] * current_multiplier_raw) % module;
          current_multiplier_raw = (simple_number1 * current_multiplier_raw) % module;
        }
        temp_hash_matrix[i][j] = sum;
      }
    }

    vector<vector<int>> hash_matrix(n - k + 1, vector<int>(m - l + 1));

    for (int i = 0; i < n - k + 1; ++i) {
      for (int j = 0; j < m - l + 1; ++j) {
        int current_multiplier_column = simple_number2;
        int sum = 0;
        for (int t = 0; t < k; ++t) {
          sum = (sum + temp_hash_matrix[i + t][j] * current_multiplier_column) % module;
          current_multiplier_column = (simple_number2 * current_multiplier_column) % module;
        }
        hash_matrix[i][j] = sum;
      }
    }

    int first_index = 0;
    int second_index = 0;

    for (int i = 0; i < n - k + 1; ++i) {
      for (int j = 0; j < m - l + 1; ++j) {
        if (hash_matrix[i][j] == hash_of_second_image) {
          //cout << "\n\n" << i << " " << j << "\n\n";
          if (i <= (n - k + 1) / 3) {
            if (j <= (m - l + 1) / 3) {
              cout << "Северо-Запад";
            } else {
              if (j >= (m - l + 1) * 2 / 3) {
                cout << "Север";
              } else {
                cout << "Северо-Восток";
              }
            }
          } else {
            if (i >= (n - k + 1) * 2 / 3) {
              if (j <= (m - l + 1) / 3) {
                cout << "Юго-Запад";
              } else {
                if (j >= (m - l + 1) * 2 / 3) {
                  cout << "Юг";
                } else {
                  cout << "Юго-Восток";
                }
              }
            } else {
              if (j <= (m - l + 1) / 3) {
                cout << "Запад";
              } else {
                if (j >= (m - l + 1) * 2 / 3) {
                  cout << "Центр картинки";
                } else {
                  cout << "Восток";
                }
              }
            }
          }
        }
      }
    }
  } else {
    int index1 = -1;
    int index2 = -1;

    for (int i = 0; i < n - k + 1; ++i) {
      for(int j = 0; j < m - l + 1; ++j) {
        int flag = 1;
        for (int t = 0; t < k && flag; ++t) {
          for (int q = 0; q < l && flag; ++q) {
            if (first_image[i + t][j + q] != second_image[t][q]) {
              flag = 0;
            }
          }
        }
        if (flag) {
          cout << "Точные координаты левого верхнего края: [" << i << ", " << j << "]\n"; 
        }
      }
    }
  }

}